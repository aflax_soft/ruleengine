# Intro #

This project is a simple project which allows payment execution depending on product type using
a small rule engine.

### Framework used in this project ###

* Java 8
* Maven
* Spring boot
* Swagger
* J-easy [J-easy](https://github.com/j-easy/easy-rules)

### How to run the project ###

* clone the project into folder
* Navigate to the folder and run the command: mvn spring-boot:run,
* the tests can be run with the command: mvn test
* Unit test and integration can be run with the command: mvn verify -Psurefire
* When the application is ready, it can be test at this [url](http://localhost:8080/swagger-ui.html)
* Just follow the instructions on the page to send the correct payload. It is important to note that there are five product type currently 
available. These are the followings: 
* PHYSICAL
* BOOK
* MEMBERSHIP
* UPGRADE
* VIDEO