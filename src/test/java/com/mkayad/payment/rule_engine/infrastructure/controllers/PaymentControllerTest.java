package com.mkayad.payment.rule_engine.infrastructure.controllers;

import com.mkayad.payment.rule_engine.domain.models.Payment;
import com.mkayad.payment.rule_engine.domain.models.PaymentAction;
import com.mkayad.payment.rule_engine.domain.models.Product;
import com.mkayad.payment.rule_engine.domain.models.ProductType;
import com.mkayad.payment.rule_engine.domain.services.IRuleEvaluatorService;
import org.jeasy.rules.api.Fact;
import org.jeasy.rules.api.Facts;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest
class PaymentControllerTest {

    @MockBean
    private IRuleEvaluatorService iRuleEvaluatorService;

    private Facts facts = new Facts();

    private PaymentAction paymentAction = new PaymentAction();
    private Collection<String> actions = Arrays.asList("SpringBootIntegrationTest","B");
    protected PaymentController paymentController;
    Payment payment = new Payment(new Product("Test", ProductType.PHYSICAL));
    @BeforeEach
    void setUp() {

        paymentController = new PaymentController(iRuleEvaluatorService,paymentAction);
    }

    @Test
    void should_return_a_list_of_actions() {
        when(iRuleEvaluatorService.evaluate(any(Facts.class))).thenReturn(actions);

        Collection<String> actualActions = paymentController.process(payment);
        assertEquals(actions,actualActions);
        verify(iRuleEvaluatorService,times(1)).evaluate(any(Facts.class));
    }


    @Test
    void should_return_an_empty_list_of_actions() {
        when(iRuleEvaluatorService.evaluate(any(Facts.class))).thenReturn(Collections.emptyList());

        Collection<String> actualActions = paymentController.process(payment);
        assertTrue(actualActions.isEmpty());
        verify(iRuleEvaluatorService,times(1)).evaluate(any(Facts.class));
    }
}
