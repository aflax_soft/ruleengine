package com.mkayad.payment.rule_engine.services;

import static org.junit.jupiter.api.Assertions.*;

import java.io.File;
import java.io.FileReader;
import com.mkayad.payment.rule_engine.domain.models.Payment;
import com.mkayad.payment.rule_engine.domain.models.PaymentAction;
import com.mkayad.payment.rule_engine.domain.models.Product;
import com.mkayad.payment.rule_engine.domain.models.ProductType;
import org.jeasy.rules.api.Facts;
import org.jeasy.rules.api.Rule;
import org.jeasy.rules.api.Rules;
import org.jeasy.rules.core.DefaultRulesEngine;
import org.jeasy.rules.mvel.MVELRuleFactory;
import org.jeasy.rules.support.reader.YamlRuleDefinitionReader;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mvel2.ParserContext;
import org.springframework.core.io.ClassPathResource;


class RuleEvaluatorServiceTest {

    protected Facts facts;
    protected Rule  rule;
    protected Rules rules;

    protected PaymentAction paymentAction = new PaymentAction();
    protected DefaultRulesEngine rulesEngine;
    protected MVELRuleFactory ruleFactory;

    @BeforeEach
    void setup() throws Exception {

        ParserContext parserContext = new ParserContext();
        parserContext.addImport(ProductType.class);
        parserContext.addImport(Product.class);
        parserContext.addImport(PaymentAction.class);

        ruleFactory = new MVELRuleFactory(new YamlRuleDefinitionReader(),parserContext);
        rulesEngine = new DefaultRulesEngine();

        File paymentRuleFile = new ClassPathResource(
                "test-rule.yml").getFile();
        rule = ruleFactory.createRule(new FileReader(paymentRuleFile));
        rules = new Rules();
    }


    @Test
    public void whenConditionIsTrue_thenActionShouldBeExecuted() {

        //setup
        facts = new Facts();
        facts.put("payment", new Payment(new Product("Test",ProductType.PHYSICAL)));
        facts.put("paymentAction",paymentAction);

        rules.register(rule);

        //when
        rulesEngine.fire(rules, facts);

        //assert
        assertEquals(true, rule.evaluate(facts));
        assertTrue(paymentAction.getActions().keySet().size()>0);

    }

    @Test
    public void whenConditionIsFalse_thenActionShouldNotBeExecuted(){
        //setup
        facts = new Facts();
        facts.put("payment", new Payment(new Product("Test",ProductType.VIDEO)));
        facts.put("paymentAction",paymentAction);

        rules.register(rule);

        // When
        rulesEngine.fire(rules, facts);

        //assert
        assertFalse( rule.evaluate(facts));
        assertTrue(paymentAction.getActions().keySet().size() ==0);
    }
}
