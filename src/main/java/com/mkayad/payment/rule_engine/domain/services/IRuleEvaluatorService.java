package com.mkayad.payment.rule_engine.domain.services;

import org.jeasy.rules.api.Facts;
import java.util.Collection;


/**
 * Simple interface to evaluate the rules for the payments
 */
public interface IRuleEvaluatorService {
    Collection<String> evaluate(Facts facts);
}
