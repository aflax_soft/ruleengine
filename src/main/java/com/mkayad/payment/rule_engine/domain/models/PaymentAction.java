package com.mkayad.payment.rule_engine.domain.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;
import java.util.HashMap;
import java.util.Map;

@Component
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class PaymentAction {
    Map<String,String> actions = new HashMap<>();

    public void add(String key,String value){
        actions.put(key, value);
    }

    public void clear(){
        actions.clear();
    }
}
