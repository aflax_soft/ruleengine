package com.mkayad.payment.rule_engine.domain.models;

public enum ProductType {
    PHYSICAL,
    BOOK,
    MEMBERSHIP,
    UPGRADE,
    VIDEO,

}
