package com.mkayad.payment.rule_engine.infrastructure.services;

import com.mkayad.payment.rule_engine.domain.models.PaymentAction;
import com.mkayad.payment.rule_engine.domain.models.Product;
import com.mkayad.payment.rule_engine.domain.models.ProductType;
import com.mkayad.payment.rule_engine.domain.services.IRuleEvaluatorService;
import lombok.extern.slf4j.Slf4j;
import org.jeasy.rules.api.Facts;
import org.jeasy.rules.api.Rules;
import org.jeasy.rules.api.RulesEngine;
import org.jeasy.rules.core.DefaultRulesEngine;
import org.jeasy.rules.mvel.MVELRuleFactory;
import org.jeasy.rules.support.reader.RuleDefinitionReader;
import org.jeasy.rules.support.reader.YamlRuleDefinitionReader;
import org.mvel2.ParserContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;
import java.io.File;
import java.io.FileReader;
import java.util.Collection;


@Slf4j
@Component
public class RuleEvaluatorServiceImpl implements IRuleEvaluatorService {

    private Rules paymentRules = new Rules();

    public RuleEvaluatorServiceImpl(){
        ParserContext parserContext = new ParserContext();
        parserContext.addImport(ProductType.class);
        parserContext.addImport(Product.class);
        parserContext.addImport(PaymentAction.class);
        RuleDefinitionReader ruleDefinitionReader = new YamlRuleDefinitionReader();
        MVELRuleFactory ruleFactory = new MVELRuleFactory(ruleDefinitionReader,parserContext);

        try {
            File paymentRuleFile = new ClassPathResource(
                    "payment-rules.yml").getFile();

            paymentRules = ruleFactory.createRules(new FileReader(paymentRuleFile));
            log.info("Importing payment rule file "+paymentRuleFile.getName());

        } catch (Exception e) {
            e.printStackTrace();
        }
        log.info("Number of paymentRules found: {} ", paymentRules.size());

    }



    public Collection<String> evaluate(Facts facts){
        // fire paymentRules on known facts
        RulesEngine rulesEngine = new DefaultRulesEngine();
        rulesEngine.fire(paymentRules, facts);
        PaymentAction paymentAction = facts.get("paymentAction");
        return paymentAction.getActions().values();
    }
}
