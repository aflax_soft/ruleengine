package com.mkayad.payment.rule_engine.infrastructure.config;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Arrays;
import java.util.Set;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


@EnableSwagger2
@Configuration
public class SwaggerConfig {
    private Set<String> DEFAULT_PRODUCES_AND_CONSUMES = new HashSet(Arrays.asList("application/json", "application/xml"));
    private Contact DEFAULT_CONTACT = new Contact("Moussa", "https://xyz.com", "mkayad@gmail.com");
    private ApiInfo DEFAULT_API_INFO = new ApiInfo(
            "Payment Management System",
            "Payment Management System API documentation",
            "1.0",
            "Terms of Service",
            DEFAULT_CONTACT,
            "Apache License Version 2.0",
            "https://www.apache.org/license.html",
            new ArrayList());

    @Bean
    public Docket serviceAPI(){
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.mkayad.payment.rule_engine"))
                .build()
                .apiInfo(DEFAULT_API_INFO)
                .produces(DEFAULT_PRODUCES_AND_CONSUMES);
    }



}
