package com.mkayad.payment.rule_engine.infrastructure.controllers;

import java.util.Collection;
import com.mkayad.payment.rule_engine.domain.models.Payment;
import com.mkayad.payment.rule_engine.domain.models.PaymentAction;
import com.mkayad.payment.rule_engine.domain.services.IRuleEvaluatorService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.jeasy.rules.api.Facts;
import org.springframework.web.bind.annotation.*;


@Api(value = "Payment controller",
        description = "process payments for different product types")
@Slf4j
@RequestMapping("/payments")
@RestController
public class PaymentController {
    private IRuleEvaluatorService ruleEvaluatorService;
    private PaymentAction paymentAction;

    public PaymentController(IRuleEvaluatorService ruleEvaluatorService,
                             PaymentAction paymentAction){
        this.ruleEvaluatorService = ruleEvaluatorService;
        this.paymentAction = paymentAction;
    }

    @ApiOperation("executes a different action depending on the product type for the submitted payment")
    @RequestMapping(method = RequestMethod.POST,
            value = "/process",
            consumes = "application/json",
            produces = "application/json")
    public @ResponseBody Collection<String> process(@RequestBody Payment payment){
        paymentAction.clear();
        Facts facts = new Facts();
        facts.put("payment",payment);
        facts.put("paymentAction",paymentAction);
        return ruleEvaluatorService.evaluate(facts);
    }
}
