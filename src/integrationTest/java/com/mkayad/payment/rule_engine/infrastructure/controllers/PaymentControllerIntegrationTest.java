package com.mkayad.payment.rule_engine.infrastructure.controllers;

import com.mkayad.payment.rule_engine.domain.models.Payment;
import com.mkayad.payment.rule_engine.domain.models.Product;
import com.mkayad.payment.rule_engine.domain.models.ProductType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Collection;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class PaymentControllerIntegrationTest {
    protected RestTemplate restTemplate = new RestTemplate();
    protected HttpHeaders headers = new HttpHeaders();

    protected final String url = "http://localhost:8080/payments/process";

    @Test
     void should_return_success_code_and_correct_actions(){
        Payment payment = new Payment(new Product("Test", ProductType.PHYSICAL));
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

        ResponseEntity<Collection<String>> response=restTemplate.exchange(url,
                HttpMethod.POST, new HttpEntity(payment,headers), new ParameterizedTypeReference<Collection<String>>(){});
        Assertions.assertEquals(response.getStatusCode(), HttpStatus.OK);
        Collection<String> actions=response.getBody();
        Assertions.assertNotNull(actions);
        Assertions.assertTrue(actions.contains("generate a packing slip for shipping!"));
    }

    @Test
    void should_return_success_code_and_empty_actions(){
        Payment payment = new Payment(new Product("Test", ProductType.VIDEO));
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

        ResponseEntity<Collection<String>> response=restTemplate.exchange(url,
                HttpMethod.POST, new HttpEntity(payment,headers), new ParameterizedTypeReference<Collection<String>>(){});
        Assertions.assertEquals(response.getStatusCode(), HttpStatus.OK);
        Collection<String> actions=response.getBody();
        Assertions.assertNotNull(actions);
        Assertions.assertTrue(actions.isEmpty());
    }
}
